@extends('layouts.master')

@section('mainContent')

    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <h5 class="text-center display-4">Tasks List</h5>
                <div class="list-group">
                    @foreach($tasks as $task)
                        <div data-id="{{ $task->id }}" class="list-group-item list-group-item-action">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1"><a href="{{ route('task.show', $task->id) }}">{{ $task->title }}</a></h5>
                            </div>
                            <p class="mb-1">{{ $task->desc }}</p>
                            <button class="btn-danger btn btn-sm delete-task">Delete</button>
                            <a href="{{ route('task.edit', $task->id) }}" class="btn-secondary btn btn-sm edit-task" role="button">Edit</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
