@extends('layouts.master')

@section('mainContent')

    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <h3>Create a new Task</h3>
                <form id="create-task-form">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" placeholder="Task Title" required>
                    </div>
                    <div class="form-group">
                        <label for="desc">Description</label>
                        <textarea type="text" class="form-control" id="desc" placeholder="Title Description" required></textarea>
                    </div>
                    <button type="button" class="btn btn-primary" id="create-task">Save</button>
                </form>
                <div id="alerts" class="mt-3"></div>
            </div>
        </div>
    </div>

@endsection
