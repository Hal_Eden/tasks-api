@extends('layouts.master')

@section('mainContent')

    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="card m-auto" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $task->title }}</h5>
                        <p class="card-text">{{ $task->desc }}</p>
                        <a href="{{ route('edit', $task->id) }}" class="btn-secondary btn btn-sm edit-task" role="button">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
