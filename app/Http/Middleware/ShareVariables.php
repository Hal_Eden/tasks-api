<?php

namespace App\Http\Middleware;

use Closure;

class ShareVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth()->check()) {
            $accessToken = auth()->user()->token;
            \View::share('accessToken', $accessToken);
        }

        return $next($request);
    }
}
