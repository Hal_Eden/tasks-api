<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user();

        if(!$user->token) {
            $token = $user->createToken($user->email)->accessToken;
            $user->token = $token;
            $user->save();
        }

        $accessToken = $user->token;

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://tasksapi.ec/api/tasks', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$accessToken,
            ],
        ]);

        $tasks = json_decode($response->getBody())->data;

        return view('pages.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accessToken = auth()->user()->token;

        return view('pages.create', compact('accessToken'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accessToken = auth()->user()->token;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://tasksapi.ec/api/tasks/' . $id, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$accessToken,
            ],
        ]);

        $task = json_decode($response->getBody())->data;

        return view('pages.task', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accessToken = auth()->user()->token;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://tasksapi.ec/api/tasks/' . $id, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$accessToken,
            ],
        ]);

        $task = json_decode($response->getBody())->data;

        return view('pages.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
