<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'share.vars'])->group(function () {
    //Show All Tasks Of The Current User
    Route::get('/tasks', 'MainController@index');
    Route::get('/', 'MainController@index');

    //Show Form To Create A New Single Task
    Route::get('/tasks/create', 'MainController@create')->name('task.create');

    //Show Single Task Of The Current User
    Route::get('/tasks/{id}', 'MainController@show')->name('task.show');

    //Show Form To Edit An Existing Task
    Route::get('/tasks/{id}/edit', 'MainController@edit')->name('task.edit');
});
