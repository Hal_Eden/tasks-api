$(document).ready(function() {

    // Get Access Token Of The Current User
    var accessToken = $("#token").val();

    // Create A Single Task
    $(document).on("click", "#create-task", function() {

        // Get Input Values
        var data = {
            "title": $('#title').val(),
            "desc": $('#desc').val()
        };

        // Make A Call To API With Access Token
        $.ajax({
            url: "http://tasksapi.ec/api/tasks",
            method: "POST",
            data: data,
            dataType: "json",
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + accessToken
            },
            success: function() {
                // Reset Inputs
                $('#create-task-form').trigger("reset");

                // Show Success Message
                var status = $("<strong><strong>").text("Success! ");
                var alert = $("<div></div>").text("The task has been created.").prepend(status);
                alert.addClass("alert alert-success");
                $('#alerts').html(alert);
            },
            error: function() {
                // Show Failure Message
                var status = $("<strong><strong>").text("Failure! ");
                var alert = $("<div></div>").text("Something went wrong. Please try again later.").prepend(status);
                alert.addClass("alert alert-danger");
                $('#alerts').html(alert);
            },
            done: function() {
            }
        });

    });

    // Edit A Single Task
    $(document).on("click", "#edit-task", function() {

        // Get The Task ID
        var taskId = $(this).data('id');

        // Get Input Values
        var data = {
            "title": $('#title').val(),
            "desc": $('#desc').val()
        };

        // Make A Call To API With Task ID And Access Token
        $.ajax({
            url: "http://tasksapi.ec/api/tasks/" + taskId,
            method: "PUT",
            data: data,
            dataType: "json",
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + accessToken
            },
            success: function() {
                // Show Success Message
                var status = $("<strong><strong>").text("Success! ");
                var alert = $("<div></div>").text("The task has been edited.").prepend(status);
                alert.addClass("alert alert-success");
                $('#alerts').html(alert);
            },
            error: function() {
                // Show Failure Message
                var status = $("<strong><strong>").text("Failure! ");
                var alert = $("<div></div>").text("Something went wrong. Please try again later.").prepend(status);
                alert.addClass("alert alert-danger");
                $('#alerts').html(alert);
            },
            done: function() {
            }
        });

    });

    // Delete A Single Task
    $(document).on("click", ".delete-task", function(e) {

        // Get The Task ID
        var taskId = $(this).parent().data('id');
        var data = {};

        // Make A Call To API With Task ID And Access Token
        $.ajax({
            url: "http://tasksapi.ec/api/tasks/" + taskId,
            method: "DELETE",
            data: data,
            dataType: "json",
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + accessToken
            },
            success: function() {
                // Remove The Task Node After Success
                $('.list-group').find('[data-id=' + taskId + ']').remove();
            },
            done: function() {
            }
        });

    });

});