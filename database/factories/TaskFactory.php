<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'desc' => $faker->text()
    ];
});
